Introduction to 6LoWireless
============================

The Amatis 6LoWireless Application Protocol is a lightweight, stateless, and robust application construct allowing devices of all types to interact in a wireless mesh network.

6LoWireless’s network doesn’t rely on a single central hub, but instead bounces messages from one device to another. This allows devices on opposite sides of the mesh to communicate even if they are out of each other's direct range. 6Lo has the added advantage of being easy and cheaply expandable, allowing for the addition of more devices without the replacement of a central hub. 6LoWireless implementation can also be installed far more quickly and cost effectively than wired alternatives. 6Lo is built for integration into commercial systems without interfering with WiFi or other wireless systems. In addition, all messages sent through the network are encrypted using AES 128 bit encryption.

It was built specifically for the lighting controls industry with large-scale commercial applications, but is easily extended to nearly any application.

6LoWireless is heavily influenced by the following core tenants:
    - Must be lightweight 
        - 6LoWireless is intended to be installed on constrained devices, so the application layer should not only use the least amount of memory as possible, but it must also consider a default state of minimal usage, where more complicated devices can be 'heavier', but the initial state must be extremely small.
    - Must be as stateless as possible
        - Due to the nature of wireless communications, it is important to minimize the amount of network traffic required for such an application layer to work. For this reason 6Lo is as stateless as possible. This means that trigger devices should not be required to know anything (or as little as possible) about the device(s) they are bound to. 
    - Must be extensible
        - 6Lo is made up of many very small and simple elements which work together to create a complicated system. Action functions (AF_) are the best example of this. Any device can easily 'learn a new skill' by simply adding an AF to its AF Table, and via the linkId binding strategy, any other device in the network can interact with that new AF without needing to know anything about it (i.e. without needing a firmware upgrade itself).
    - Must be based on common open standards and practices
        - Although 6Lo does use some open source libraries, what is more important is that it strives to use common internet protocol technologies and architecture (i.e. RESTful interfaces, TLS, UDP...)

These documents will begin to explain the many components that create the 6LoWireless system.


`amatiscontrols.com
<http://www.amatiscontrols.com>`_
