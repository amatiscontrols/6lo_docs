Scenes
======
Scenes are a grouping of 'triggers' and 'actions'. All AF_ s in a given scene *have the same linkId*

Scenes are represented as objects with references to their associated devices. They can be constructed by parsing each device’s AFT_ (action function table), despite AFTs not having references to the other devices in the scene. By using linkId as the common index that relates AFs together, we are left with a clear description of the cause and effect of any event on the network.

The following is how the Amatis app displays scenes for editing. Be aware that the elements that make up a given AF are being shown in a more readable representation than they are stored in the devices' ATFs.

.. image:: images/scene_example.png

Here we see two different devices connected by a common linkId, 262, which is shown underneath the scene name. In this case the master driver acts as both a trigger and action device. Every device in this scene has a reference to linkId 262 in its AFT. 

This scene contains the following instructions:

When the master driver receives a motion sense signal, it should trigger itself and driver 473a to tell their linked fixtures to go to 100% over a two second fade period. They should then listen for daylight harvesting data to determine if they should fade back down.

The following is a single AF from the AFT of driver 473. You can see that linkId 262 references this external AF. var1 data is internally stored as machine readable data but here on the AFT a human-centric translation is available. It reads as 'Set level 50%, 2s fade & enable DLH'. This information, and that of other devices, is parsed to create the app display seen above. See the section on action function rows (AFR_) for a deeper explanation on the meaning of this AFR.

.. image:: images/scene_example_af.png



.. _AF: action_functions.html
.. _AFT: action_function_table.html
.. _AFR: action_function_table.html
