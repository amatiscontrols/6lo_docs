Action Function Table
======================

Action Function Table (AFT):
-----------------------------
**NOTE: The following image is a human readable version of what is actually stored in the device. The device table can be directly read using the CoAP endpoint / table_list**.

.. image:: images/action_function_table.png

This table creates distributed stateless links between devices. Sending and receiving devices both reference the same table. **No event that occurs inside a device can be communicated to any other device on the network without first passing through this table**

A device sending an event message (EM) into the network interacts with the action table in the opposite direction that a device receiving an EM will. 

.. image:: images/sending_device_em.png
.. image:: images/receiving_device_em.png

Action Function Rows (AFR):
----------------------------

The following are the most important three components when seeking to understand a single AFR:
    1) linkId
    2) var1
    3) The Action Function Name

If we analyze a single line of the table above, row 15, we see the following breakdown:

.. code-block:: javascript

    { linkId: 1979, var1: 1001, AF_name: Set Auto Duty(%,Fade)}

What row 15 tells the device is, "If I hear an Event Message with linkId 1979, I should use my Set Auto Duty skill, passing in the variable 1001."

For this particular AF, var1 is a map of 6 digits representing 3 for the target level, and 3 for the fade rate to be used to get there (both as decimal values). 

To decode the example above, *1001* is really *001001* with leading 0s added back. *001001* means the device should go to a **target level of 001% (1%), over a 001s (1s) fade rate.**

As previously mentioned, Scenes_ are a grouping of triggers and actions between one or more devices. This means that another device has a row in its ATF that tells it to call linkId 1979.

Trigger Action Function Rows (TAFR):
-------------------------------------
Trigger Action Function Rows (TAFR) fit into the same structure, but look a little bit different. Let's look at a different devices AFT as an example:

.. image:: images/action_function_trigger_row.png

.. code-block:: javascript

    { linkId: INT.Motion, var1: 1984, AF_name: coap_send_action(linkID)}

.. TODO: Update photo and explanation

**NOTE: The linkId is now a string. This actually references an internal linkId, which we will circle back to.**

This row as a sentence reads: "When I determine that I have detected a new motion event, I will send out an Event Message with  linkId 1986."

TAFRs look a little bit different because the binding is actually to an **Internal Action Function** which means it can only be triggered by the firmware itself, and the variable (var1), is actually the linkId that binds this internal event to the action table of another device.

Summary:
--------
It is much less important to memorize all of the acronyms above then it is to understand the following simple concepts:

1) The Action Function Table exists in all devices, and is the sole mechanism for a device to decide how to, and if it should, interact with other devices on a network.
2) The 'bindings' created between two devices via a shared linkId reference in each of their action tables is stateless. A device sending an EM knows nothing about the state of the device it is communicating with.
3) Although no concepts such as groups, rooms, scenes, or locations are directly represented in the AFT, the relationship of every device, to any other device in the network can be determined using these tables alone. 

.. _Scenes : scenes.html
