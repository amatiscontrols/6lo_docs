Data Dictionary
================

The Data Dictionary (DD) stores configuration parameters, energy totalizers, and other state parameters such as power consumption and output level for 6LoWireless devices.

Although relationships between events and actions are stored in the action function table (AFT), the DD compliments it by providing the location for parameters such as 'High End Trim', 'Occupancy State', and 'Daylight Harvesting Tuning'.

The DD supports 64-bit variables to allow up to 20 years of energy totalization before roll-over.

Structure
----------

.. code-block:: javascript

    {"idx":"0","dname":"RSSI","cval":"-34e0","un":"dbm"}
    {"idx":"10","dname":"Channel 1","cval":"0e0","un":"%"}
    {"idx":"19","dname":"Baseline Wh","cval":"19086656e-2","un":"Wh"}
    {"idx":"20","dname":"Energy","cval":"6437630e-2","un":"Wh"}
    {"idx":"22","dname":"PIR Value","cval":"0e0","un":"state"}
    {"idx":"26","dname":"Timeout Mode","cval":"0e0","un":"state"}
    {"idx":"28","dname":"Light","cval":"1e0","un":"%"}]

Requesting It
---------------

.. role:: javascript(code)
   :language: javascript

A :javascript:`GET` request to the coap endpoint :javascript:`datamanagerlog` is used to query a device for its data dictionary. It takes the following parameters:

- force
    - ex: :javascript:`datamanagerlog?force=1`
    - This is an integer value between 1 and 7 currently
    - If no force parameter is passed, the default value of  :javascript:`?force=1` will be used
- start 
    - ex: :javascript:`datamanagerlog?force=1&start=4`
    - This is an integer value that represents the first :javascript:`idx` to be returned
    - If no :javascript:`stop` parameter is included, only the datapoint with idx matching the start parameter will be returned.
- stop
    - ex: :javascript:`datamanagerlog?force=1&start=4&stop=6`
    - This is an integer value that represents the last :javascript:`idx` to be returned
    - In this instance, data points with idx 4,5,6 (3 total) will be returned. Note, if an idx is requested but does not exist in the DD, that datapoint will simply not be returned, or an empty array will be returned.

Verbosity
----------
Verbosity relates to the :javascript:`force` parameter that it’s passed in. The following levels of verbosity in response are available:

1) Return all meaningful data points
2) Return every datapoint in the DD, across all force values
3) Return every datapoint in the DD, without names or units
4) Returns an empty array for the one i'm working with
5) Return only the data to be logged every 5 minutes
6) Return only writeable points
7) Return advanced/hidden config
