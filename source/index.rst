6Lo Wireless Documentation
=============================================

Welcome to the begining of the 6LoWireless protocol documentation effort

.. toctree::
   :maxdepth: 7
   :caption: Contents:

   intro
   scenes
   action_functions
   action_function_table
   data_dictionary
   erics_page
   overview.rst

   


