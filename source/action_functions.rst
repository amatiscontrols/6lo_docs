Action Functions
================

Overview
--------
- Action functions (AF) are like skills. They are quite simply *things that a device knows how to do.*
- Every type of device has its own set of AFs.
- AFs intended to be executed at run time are stored in the action function table (AFT_) of each device individually.
- Some AFs are present in all devices. We call these "core internal action functions".
- There are two main groupings of AFs:
    - Internal AFs
        - These AFs are baked into the firmware for any device and always have a linkId less than 256.
    - External AFs
        - These AFs are programmed directly by a user or commissioning agent. They can also be created automatically by devices at run time using the push to pair (p2p) strategy.

Example AFs:
------------
    - Dimmers/Controllers:
        - Set Manual Duty(%,Fade)
            - Goes to a particular level, over a given fade rate, and ignores input from any daylight harvest groups it may be associated with.
        - Set Auto Duty(%,Fade)
            - Goes to a particular level, over a given fade rate, then listens for DLH information.
        - Enable Relay(0/1)
            - Close or open the relay depending on the var1 - 0 opens it, 1 closes it.
        - Daylight Link
            - Links a controller to a given daylight sensor and converts the daylighting information it receives into a tangible level change (or lack thereof) for the controller.
    - Sensors:
        - Daylight Harvest
            - Causes a daylight capable sensor to call the applicable scene via its linkId, when measured light levels move past a certain threshold. It sends the current value out to bound controllers to interpret into a level change, if needed.
        - coap_send_action(linkID)
            - This tells the device to multicast a particular linkId. This can be used to 'daisy chain' events, or simply to schedule a particular action. This function is effectively the same one that is used anytime a Trigger Event occurs (i.e. motion).

Structure:
----------
    - Each Action Function Row (AFR) has the following properties.
        - lId
            - This is the 6Lo linkId that is used to reference this row.
        - F
            - This is a flag that is not currently used by the system, but could be implemented to add more functionality to the existing structure.
        - FM
            - Is a flag mask that would work in harmony with the flag property if it were implemented to make AFRs more customizable.
        -  Val
            - The variable that is passed to the action function. We often refer to it in code as *`var1`*.
        - aFun
            - A memory reference to the action function being called. Amatis uses a cloud reference table that is built out by asking a device for its function_list (a human readable list of AFs) before asking for its AFT.

**Example AF directly from a device:**

.. code-block:: javascript

    {"lId":"0x3","F":"0xFFFF","FM":"0xFFFF","Val":"0x0","aFun":"0x1051F5"}

*Note: The names of these properties as they are returned by the coap engine are intentionally not verbose. Although they still could be named better than they are, every character counts when transmitting ascii over the 15.4 network.*


.. _AFT: action_function_table.html
